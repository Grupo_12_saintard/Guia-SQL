## super librerias
import pyodbc
import textwrap
import pandas as pandas
import random
from datetime import date
from datetime import datetime

## definiendo el server y usuario
server = "127.0.0.1"
database = "DB_test"
username = "sa"
password = "<YourStrong!Passw0rd>123"

### conexion y creando el cursor
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

# Definiendo matriz que se enviara
matriz = []
cantidad = int(input("cantidad de datos a agregar al servidor:  "))
for i in range(cantidad):
    lista = []
    lista.append(str("{0:.2f}".format(random.uniform(0, 100))))
    lista.append((str(random.randint(0,100))+'%'))
    lista.append(str(datetime.now()))
    matriz.append(lista)

print(matriz)


# ciclo for para cada valor
for index, row in enumerate(matriz):
    
    # intentando meter cada valor en su lugar
    insert_query = textwrap.dedent('''
        INSERT INTO esquemaunico.tiempos (Temperature, Humedad, Sampletime) 
        VALUES (?, ?, ?);
    ''')
    
    # definiendo los valores
    values = (row[0], row[1], row[2])
    
    #poniendo los datos
    cursor.execute(insert_query, values)

# el commit
cnxn.commit()
    
# muestra lo que meti
cursor.execute('SELECT * FROM esquemaunico.tiempos')
for row in cursor:
    print(row)
    
# cierra todo 
cursor.close()
cnxn.close()