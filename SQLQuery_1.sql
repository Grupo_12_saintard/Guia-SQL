USE master
GO

IF NOT EXISTS (
    SELECT [name]
        FROM sys.databases
        WHERE [name] = N'DB_test'
)
CREATE DATABASE DB_test
GO

USE DB_test
GO

-- super tablita

CREATE TABLE esquemaunico.tiempo (
    Id_sensor INT IDENTITY (1, 1) PRIMARY KEY,
    Temperature VARCHAR(30),
    Humedad VARCHAR(30),
    Sampletime VARCHAR(30)
);